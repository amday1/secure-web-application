# SE2CWK

Repository containing material for Software Engineering 2 Module of UEA BSc Computer Science.
Task:
The aim of this assignment is to create a secure web application that mitigates the common 
vulnerabilities, for example: SQL injection, cross-site scripting and cross-site request forgery. 
You will be required to then appraise the security of a web application developed by 
another group. 
You are expected to use Python and Flask and to write your own code to protect against the 
various vulnerabilities rather than using the available functionality packaged with Flask. 

Team:
Alexander Day,
Sebastian Papararo,
Adam Loraine

