import os
import sqlite3

from flask import Flask
from flask_bcrypt import Bcrypt

DATABASE = 'database.db'
app = Flask(__name__)
bcrypt = Bcrypt(app)


def create():
    db = sqlite3.connect(DATABASE)

    c = db.cursor()

    c.execute('''
        CREATE TABLE users (
	        email varchar UNIQUE,
	        username varchar PRIMARY KEY,
	        password varchar,
	        created_on varchar,
	        verified DEFAULT 0,
	        uuid varchar UNIQUE
        );
    ''')

    c.execute('''
        CREATE TABLE posts (
	        postid integer PRIMARY KEY AUTOINCREMENT,
	        posted_by varchar,
	        title varchar,
	        content varchar,
	        posted_on varchar
        );
    ''')

    c.execute('''
        CREATE TABLE forgotPasswordRequests (
            id varchar PRIMARY KEY,
            email varchar
        );
    ''')

    hashedPass = bcrypt.generate_password_hash('adamADAM123!').decode('utf-8')
    query = 'INSERT INTO users VALUES("a.j.loraine@gmail.com","adam","%s","19/11/2018", 1, "test1")' % (hashedPass)
    c.execute(query)
    hashedPass = bcrypt.generate_password_hash('maxMAX123!').decode('utf-8')
    query = 'INSERT INTO users VALUES("a.max.day@gmail.com","max","%s","19/11/2018", 1, "test2")' % (hashedPass)
    c.execute(query)
    hashedPass = bcrypt.generate_password_hash('sebSEB123!').decode('utf-8')
    query = 'INSERT INTO users VALUES("s.papararo@gmail.com","seb","%s","19/11/2018", 1, "test3")' % (hashedPass)
    c.execute(query)

    c.execute(
        '''INSERT INTO posts VALUES('1', 'max', 'This is the first post', 'It is a test post', '19/11/2018 at 12:31');''')
    c.execute(
        '''INSERT INTO posts VALUES('2', 'adam', 'WOW!', "I can't believe how secure this website is!", '19/11/2018 at 12:39');''')
    c.execute(
        '''INSERT INTO posts VALUES('3', 'seb', 'Incredible Stuff Jeff', 'The website is just so functional and looks good as well!', '19/11/2018 at 12:43');''')

    db.commit()


def delete_db():
    if os.path.exists(DATABASE):
        os.remove(DATABASE)


if __name__ == '__main__':
    delete_db()
    create()
