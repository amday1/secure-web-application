import datetime
import json
import re
import secrets
import sqlite3
import urllib
import uuid
from string import ascii_uppercase, ascii_lowercase, digits

from flask import *
from flask_bcrypt import Bcrypt
from flask_mail import Mail, Message

app = Flask(__name__)
app.secret_key = secrets.token_hex(64)
bcrypt = Bcrypt(app)

app.config['SESSION_COOKIE_SECURE'] = True
app.config['BCRYPT_HANDLE_LONG_PASSWORDS'] = True
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'se2cwk@gmail.com'
app.config['MAIL_PASSWORD'] = 'sebSEB123!'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail = Mail(app)
DATABASE = 'database.db'



## Database Methods
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)

    def make_dicts(cursor, row):
        return dict((cursor.description[idx][0], value)
                    for idx, value in enumerate(row))

    db.row_factory = make_dicts
    return db


def query_db(query, args=(), one=False):
    cur = None
    rv = None
    try:

        cur = get_db().execute(query, args)
        rv = cur.fetchall()
    except sqlite3.Error as e:
        app.logger.info('Database error: %s' % e)
    except Exception as e:
        app.logger.info('Exception in query_db: %s' % e)
    finally:
        if cur:
            cur.close()
    return (rv[0] if rv else None) if one else rv


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# Method to sanitise the inputs by replacing suspect characters
def sanitiseInputs(input):
    input = input.replace('"', '&#34')
    input = input.replace('#', '&#35')
    input = input.replace('&', '&#38')
    input = input.replace("'", '&#39')
    input = input.replace('(', '&#40')
    input = input.replace(')', '&#41')
    input = input.replace('/', '&#47')
    input = input.replace(';', '&#59')
    input = input.replace('<', '&#60')
    return input.replace('>', '&#62')


# Used to densanitise inputs so the edit post text displays correctly and it doesn't resanitise already sanitised text
def desanitiseInputs(input):
    input = input.replace('&#62', '>')
    input = input.replace('&#60', '<')
    input = input.replace('&#59', ';')
    input = input.replace('&#47', '/')
    input = input.replace('&#41', ')')
    input = input.replace('&#40', '(')
    input = input.replace('&#39', "'")
    input = input.replace('&#38', '&')
    input = input.replace('&#35', '#')
    return input.replace('&#34', '"')


# method to check any redirects to ensure they stay within the site and warm them if they do
def is_relative(url):
    return re.match(r"^\/[^\/\\]", url)


@app.route('/')
@app.route('/index/')
def index():
    # Query that gets the postid, title, content, and the user it was posted by and pass the result to the template
    query = "SELECT posts.postid, posts.title, posts.content, posts.posted_by, posts.posted_on FROM posts"
    result = query_db(query)
    return render_template('index.html', data=result)


@app.route('/dashboard/')
def dashboard():
    # Only allow access to the dashboard page to if logged in
    if session:
        # check that the query returns something first - if not just don't show anything.
        if query_db('SELECT verified FROM users WHERE username = "%s"' % session['username']):
            verified = query_db('SELECT verified FROM users WHERE username = "%s"' % session['username'])[0].get(
                'verified')
            # check to see if user has verified their email and prompt them to do so if they have not
            if verified != 1:
                flash('You still haven\'t verified your email address. Check your emails!')
        # Query that gets the postid, title, content, and the user it was posted by and pass the result to the template
        query = 'SELECT postid, title, content, posted_by, posted_on FROM posts WHERE posted_by = "%s"' % (
            session['username'])
        result = query_db(query)
        return render_template('dashboard.html', data=result, verified=verified)
    else:
        # make page for no access, go back
        flash('Log in to see this page!')
        return redirect('/login')


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # If the fields are not empty
        username = sanitiseInputs(request.form['username'])
        if username != '' and request.form['password'] != '':
            # if the user exists and password is correct
            if query_db('SELECT COUNT(username) FROM users WHERE username = "%s"' % (username)) and \
                            query_db('SELECT COUNT(username) FROM users WHERE username = "%s"' % (
                                    username)) \
                                    [0].get('COUNT(username)') == 1 and \
                    bcrypt.check_password_hash(query_db('SELECT password FROM users WHERE username="%s"' %
                                                                (username))[0].get('password'),
                                               request.form['password']):
                # If the captcha is valid
                if verifyCaptcha():
                    session['username'] = username
                    session.permanent = True
                    app.permanent_session_lifetime = datetime.timedelta(minutes=1440)
                    return redirect('/dashboard')
                else:
                    flash('Invalid Captcha!')
            else:
                flash('The username or password you entered is incorrect!')
        else:
            flash('The username and password fields cannot be left blank!')

    return render_template('login.html')


@app.route('/logout/')
def logout():
    session.pop('username', None)
    return redirect('/index')


def contains(required_chars, s):
    return any(c in required_chars for c in s)


def contains_upper(s):
    return contains(ascii_uppercase, s)


def contains_lower(s):
    return contains(ascii_lowercase, s)


def contains_digit(s):
    return contains(digits, s)


def contains_special(s):
    return contains(r"""!@$%^&*()_-+={}[]|\,.></?~`"':;""", s)


def long_enough(s):
    return len(s) >= 8


def validate_password(password):
    error = None
    # list of all possible errors with password input
    VALIDATIONS = (
        (contains_upper, 'Password needs at least one upper-case character.'),
        (contains_lower, 'Password needs at least one lower-case character.'),
        (contains_digit, 'Password needs at least one number.'),
        (contains_special, 'Password needs at least one special character.'),
        (long_enough, 'Password needs to be at least 8 characters in length.'),
    )
    failures = [
        msg for validator, msg in VALIDATIONS if not validator(password)
    ]
    if not failures:
        return True
    else:
        for msg in failures:
            flash(msg)
        return False


# need to implement checking username before creating a user
@app.route('/create_account/', methods=['POST', 'GET'])
def create_account():
    if request.method == 'POST':
        if re.match('^[_a-z0-9-]+(.[_a-z0-9-]+)+@[a-z0-9-]+(.[a-z0-9-]+)(.[a-z]{2,4})$', request.form['email']):
            if query_db('SELECT COUNT(email) FROM users WHERE email = "%s"' % (request.form['email'])) \
                    [0].get('COUNT(email)') == 0:
                username = sanitiseInputs(request.form.get('username'))
                if re.match("^[A-Za-z0-9_-]*$", username):
                    # if none of the fields are empty
                    if username != '' and request.form.get('password') != '' and request.form.get(
                            'passwordcheck') != '':
                        # If the user does not exist continue
                        if query_db('SELECT COUNT(username) FROM users WHERE username = "%s"' % (username)) \
                                [0].get('COUNT(username)') == 0:
                            # If the passwords match
                            if request.form.get('password') == request.form.get('passwordcheck'):
                                # If the password meets all the password rules
                                if validate_password(request.form.get('password')):
                                    # If the captcha is valid
                                    if verifyCaptcha():
                                        # Generate random uuid
                                        id = str(uuid.uuid4())
                                        # Hash an ID to store for verifying the email
                                        hashedID = bcrypt.generate_password_hash(id).decode('utf-8')

                                        # This also adds a random salt to the password so no 2 passwords will have the same hash
                                        # therefore comparing passwords and rainbow tables won't work. The only way is to
                                        # brute force the password
                                        hashedPass = bcrypt.generate_password_hash(request.form.get('password')).decode(
                                            'utf-8')
                                        query = 'INSERT INTO users(email, username, password, created_on, uuid) VALUES ("%s", "%s", "%s", "%s", "%s")' % (
                                            request.form['email'], username, hashedPass,
                                            datetime.datetime.today().strftime('%d/%m/%Y'), hashedID)
                                        result = query_db(query)
                                        get_db().commit()

                                        # send an email with a link to verify_email page with the id given
                                        link = 'https://127.0.0.1:5000/verify_email?id=%s&username=%s' % (id, username)
                                        msg = Message("Verify Email - Norfolk Music", sender="se2cwk@gmail.com",
                                                      recipients=[request.form['email']])
                                        messageBody = 'Hi %s, please click this link to verify your email: %s' % (
                                            username, link)
                                        msg.body = messageBody
                                        mail.send(msg)
                                        flash('Email has been sent')
                                        return redirect('/login/')
                                    else:
                                        flash('Invalid Captcha!')
                            else:
                                flash("Passwords don't match")
                        else:
                            flash("Invalid username. Please pick another one.")
                    else:
                        flash("Username and password fields cannot be left blank")
                else:
                    flash('Username contained invalid characters!')
            else:
                flash("Invalid email. Please pick another one.")
        else:
            flash('Invalid email!')
    return render_template('create_account.html')


@app.route('/verify_email', methods=['GET'])
def verify_account():
    # get id from url
    linkID = request.args.get('id')
    username = request.args.get('username')

    # this if statement maks sure the logged in user or the non-logged in user cannot see the error messages for verify email
    if linkID is not None:
        # check if verified already
        if query_db('SELECT verified FROM users WHERE username = "%s"' % username)[0].get('verified') == 1:
            flash('Your account is already verified!')
        else:
            # check against db
            if bcrypt.check_password_hash(
                    query_db('SELECT uuid FROM users WHERE username = "%s"' % username)[0].get('uuid'), linkID):
                # set verify in db to true
                query_db('UPDATE users SET verified = 1 WHERE username = "%s"' % username)
                get_db().commit()
                flash('The account for "%s" is now verified.' % username)
            else:
                flash(
                    'Something went wrong. Try logging in to send another email (and don\'t forget to check your spam folder!)')
        return render_template('verify_account.html')
    else:
        return redirect('/')


@app.route('/resend_verify/', methods=['POST'])
def resend_verify():
    # get uuid from database
    id = str(uuid.uuid4())
    hashedID = bcrypt.generate_password_hash(id).decode('utf-8')
    query_db('UPDATE users SET uuid = "%s" WHERE username = "%s"' % (hashedID, session['username']))
    get_db().commit()
    email = query_db('SELECT email FROM users WHERE username = "%s"' % session['username'])[0].get('email')

    # send an email with a link to verify_email page with the id given
    link = 'https://127.0.0.1:5000/verify_email?id=%s&username=%s' % (id, session['username'])
    msg = Message("Verify Email - Norfolk Music", sender="se2cwk@gmail.com",
                  recipients=[email])
    messageBody = 'Hi %s, please click this link to verify your email: %s' % (session['username'], link)
    msg.body = messageBody
    mail.send(msg)
    flash('Email has been sent')
    return redirect('/dashboard/')


# Functions that verifies a captcha attempt by asking google whether they have verified that attempt
@app.route('/verifyCaptcha', methods=["POST", "GET"])
def verifyCaptcha():
    verifyURL = 'https://www.google.com/recaptcha/api/siteverify'
    # The secret key for this captcha generated by google
    secret = '6Lc7ynsUAAAAAAJ4uajX9K_cSB3Vg8jdkPxzpXM7'

    # Encode the parameters required for the request
    params = urllib.parse.urlencode({
        'secret': secret,
        'response': request.form.get('g-recaptcha-response'),
    }).encode("utf-8")

    data = urllib.request.urlopen(verifyURL, params).read()
    # Googles response is a json file
    result = json.loads(data)
    # Success will either be true or false
    success = result.get('success', None)

    return success


@app.route('/new_post/')
def new_post():
    # if the user is verified, allow them to post
    if query_db('SELECT verified FROM users WHERE username = "%s"' % session['username'])[0].get('verified') == 1:
        return render_template('new_post.html', username=session['username'])
    else:
        flash('You must verify account before posting')
        return redirect('/dashboard')


@app.route('/create_post/', methods=['POST'])
def create_post():
    # collect post content from page
    title = request.form.get('title')
    content = request.form.get('content')
    title = sanitiseInputs(title)
    content = sanitiseInputs(content)
    # sanitise for XSS and sql injection
    # once clean - add post to db
    query = 'INSERT INTO posts (posted_by, title, content, posted_on) VALUES("%s","%s","%s","%s");' % (
        session['username'], title, content, datetime.datetime.today().strftime('%d/%m/%Y at %H:%M'))
    result = query_db(query)
    get_db().commit()

    return redirect('/index')


@app.route('/edit_post_page/', methods=['POST'])
def edit_post_page():
    # desanitises the content presented on the edit post page so is readable
    desanitisedContent = desanitiseInputs(request.form['content'])
    desanitisedTitle = desanitiseInputs(request.form['title'])

    return render_template('/edit_post.html', data=request.form, cleanTitle=desanitisedTitle,
                           cleanContent=desanitisedContent)


@app.route('/edit_post/', methods=['POST'])
def edit_post():
    # takes hidden field posted_on from from
    posted_on = request.form['posted_on']
    currentDateTime = datetime.datetime.today().strftime('%d/%m/%Y at %H:%M')
    editedDateTime = posted_on + ", edited on: " + currentDateTime

    # update db to change title/content or both, and the posted_on date
    newTitle = sanitiseInputs(request.form['title'])
    newContent = sanitiseInputs(request.form['content'])
    query_db(
        'UPDATE posts SET title="%s", content="%s", posted_on="%s" WHERE postid="%s"' % (
            newTitle, newContent, editedDateTime,
            request.form['postid']))
    get_db().commit()
    flash("Post updated successfully")
    return redirect('/dashboard')


@app.route('/delete_post/', methods=['POST'])
def delete_post():
    # if the logged in user owns the post, delete from db
    if session['username'] == request.form['postedby']:
        query = 'DELETE FROM posts WHERE postid="%s";' % (request.form['postid'])
        result = query_db(query)
        get_db().commit()
        flash("Post has been deleted")
    else:
        flash("Post could not be deleted because the post is owned by someone else")
    return redirect('/dashboard')


@app.route('/delete_account/', methods=['GET', 'POST'])
def delete_account():
    if request.method == 'POST':
        username = sanitiseInputs(request.form.get('username'))
        # if none of the fields are empty
        if username != '' and request.form.get('password') != '':
            # If the user exists continue
            if session['username'] == username:
                # If the captcha is valid
                if verifyCaptcha():
                    # remove personal data from posts
                    query = 'UPDATE posts SET posted_by = "deleted user" WHERE posted_by = "%s";' % username
                    query_db(query)
                    query = 'DELETE FROM users WHERE username = "%s";' % username
                    result = query_db(query)
                    get_db().commit()
                    session.pop('username', None)
                    return redirect('/')
                else:
                    flash('Invalid Captcha!')
            else:
                flash("Invalid username")
        else:
            flash("Username and password fields cannot be left blank")
    return render_template('dashboard.html')


@app.route('/change_password/', methods=['POST'])
def change_password():
    if request.method == 'POST':
        # check fields are not empty, else return fields are empty
        if request.form['oldpassword'] != '' and request.form['newpassword'] != '' and \
                        request.form['verifypassword'] != '':
            # check user exists and password matches database, else return username or password is not correct
            if bcrypt.check_password_hash(
                    query_db('SELECT password FROM users WHERE username="%s"' % (session['username']))[0].get(
                        'password'), request.form['oldpassword']):
                # make sure oldpassword and newpassword are not the same
                if request.form['oldpassword'] != request.form['newpassword']:
                    # make sure newpassword and verifypassword are the same
                    if request.form['newpassword'] == request.form['verifypassword']:
                        # if new password is valid, hash password and update db
                        if validate_password(request.form.get('newpassword')):
                            hashedPass = bcrypt.generate_password_hash(request.form.get('newpassword')).decode('utf-8')
                            flash('Password updated!')
                            # update db
                            query_db('UPDATE users SET password="%s" WHERE username="%s"' % (
                                hashedPass, session['username']))
                            get_db().commit()
                            # logs them out to force log in with new password
                            session.pop('username', None)
                            return redirect('/login')
                        else:
                            flash('Password does not meet requirements')
                    else:
                        flash('New password does not match verified password')
                else:
                    flash('New password cannot be the same as the new password')
            else:
                flash('The username or password you entered is incorrect')
        else:
            flash('The username and password fields cannot be left blank')
    else:
        return render_template('dashboard.html')


@app.route('/search/', methods=['GET'])
def search():
    search_term = request.args.get('search_term')
    # sanitise the inputs
    search_term = sanitiseInputs(search_term)
    # Query that gets posts that contain the specified terms
    query = 'SELECT * FROM posts WHERE title LIKE "%s" OR content LIKE "%s" OR posted_by LIKE "%s";' % (
        '%' + search_term + '%', '%' + search_term + '%', '%' + search_term + '%')
    result = query_db(query)

    return render_template('search_results.html', data=result, search_term=search_term)


@app.route('/forgot_password/', methods=['POST'])
def forgot_password():
    # Generate random uuid
    id = str(uuid.uuid4())
    # Hash an ID to store for verifying the email
    hashedID = bcrypt.generate_password_hash(id).decode('utf-8')

    if query_db('SELECT COUNT(email) FROM forgotPasswordRequests WHERE email = "%s"' % (request.form['email'])) and \
                    query_db('SELECT COUNT(email) FROM forgotPasswordRequests WHERE email = "%s"' % (
                            request.form['email']))[0].get('COUNT(email)') == 0:

        query = 'INSERT INTO forgotPasswordRequests(id, email) VALUES ("%s", "%s")' % (hashedID, request.form['email'])
        result = query_db(query)
        get_db().commit()
    else:
        query = 'UPDATE forgotPasswordRequests SET id="%s" WHERE email="%s"' % (hashedID, request.form['email'])
        result = query_db(query)
        get_db().commit()

    # check to see if there is a user in the database with the same email as was specified
    exists = query_db('SELECT COUNT(email) FROM users WHERE email = "%s"' % request.form['email'])
    if exists:
        # if the email is related to a user, send email
        # send an email with a link to verify_email page with the id given
        linkedUser = query_db('SELECT username FROM users WHERE email="%s"' % request.form['email'])[0].get('username')
        # create the link with the username instead of the email so personal information is not on show
        link = 'https://127.0.0.1:5000/reset_password?id=%s&username=%s' % (id, linkedUser)
        msg = Message("Reset Password - Norfolk Music", sender="se2cwk@gmail.com",
                      recipients=[request.form['email']])
        messageBody = 'Hi %s, please click this link to reset your password: %s' % (
            linkedUser, link)
        msg.body = messageBody
        mail.send(msg)
    # if the email is sent or not, say it has any way - account enumeration
    flash('Email has been sent')
    return redirect('/login/')


@app.route('/reset_password', methods=['GET', 'POST'])
def reset_password():
    if request.method == 'POST':
        # getting the email which corresponds to the username
        linkedEmail = query_db('SELECT email FROM users WHERE username="%s"' % (request.form['username']))[0].get(
            'email')
        # check the id in the url is valid - and that account has not been deleted since email sent
        if bcrypt.check_password_hash(
                query_db('SELECT id FROM forgotPasswordRequests WHERE email = "%s"' % linkedEmail)[0].get(
                    'id'), request.form['linkID']) and query_db(
                    'SELECT username FROM users WHERE email = "%s"' % linkedEmail):
            # make sure newpassword and newpasswordcheck are the same
            if request.form['newpassword'] == request.form['newpasswordcheck']:
                # if new password is valid, hash password and update db
                if validate_password(request.form.get('newpassword')):
                    hashedPass = bcrypt.generate_password_hash(request.form.get('newpassword')).decode('utf-8')
                    # update db
                    query_db('UPDATE users SET password="%s" WHERE email="%s"' % (hashedPass, linkedEmail))
                    get_db().commit()
                    flash('Password updated!')
                    return redirect('/login/')
                else:
                    return redirect('/reset_password?id=%s&username=%s' % (request.form['linkID'], linkedEmail))
            else:
                flash("Passwords do not match")
                return redirect('/reset_password?id=%s&username=%s' % (request.form['linkID'], linkedEmail))
        else:
            flash('Something went wrong. Try filling out the forgot password form again.')
            return redirect('/login/')
    # get method to load the pagge
    else:
        # the page must be loaded with both id and email arguments (so errors aren't display to users)
        if request.args.get('id') is not None and request.args.get('username') is not None:
            return render_template('forgot_password.html')
        else:
            return redirect('/')


if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=False, ssl_context='adhoc')
